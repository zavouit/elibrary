<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "authors".
 *
 * @property int $id
 * @property string $lname
 * @property string $fname
 * @property string $gender
 * @property string $date_of_birth
 * @property string $image
 * @property string $description
 * @property int $is_active
 * @property string $date_created
 * @property string $date_updated
 *
 * @property BookAuthors[] $bookAuthors
 * @property Books[] $books
 */
class Author extends \yii\db\ActiveRecord
{
    public $photo;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lname', 'fname', 'date_created', 'date_updated'], 'required'],
           /* [['date_of_birth', 'date_created', 'date_updated'], 'safe'],
            [['description'], 'string'],
            [['lname', 'fname', 'gender'], 'string', 'max' => 45],
            [['image'], 'string', 'max' => 255],
            [['is_active'], 'string', 'max' => 4],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lname' => 'Lname',
            'fname' => 'Fname',
            'gender' => 'Gender',
            'date_of_birth' => 'Date Of Birth',
            'image' => 'Image',
            'description' => 'Description',
            'is_active' => 'Is Active',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookAuthors()
    {
        return $this->hasMany(BookAuthors::className(), ['author_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Book::className(), ['id' => 'book_id'])->viaTable('book_authors', ['author_id' => 'id']);
    }
}
