<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "book_authors".
 *
 * @property int $book_id
 * @property int $author_id
 * @property int $is_active
 * @property string $date_created
 * @property string $date_updated
 *
 * @property Authors $author
 * @property Books $book
 */
class BookAuthors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'book_authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['book_id', 'author_id','last_updated'], 'required'],
            /*[['book_id', 'author_id'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['is_active'], 'string', 'max' => 4],
            [['book_id', 'author_id'], 'unique', 'targetAttribute' => ['book_id', 'author_id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Author::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => Book::className(), 'targetAttribute' => ['book_id' => 'id']],
        */];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'book_id' => 'Book ID',
            'author_id' => 'Author ID',
            'is_active' => 'Is Active',
            'last_updated' => 'last_updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Author::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBook()
    {
        return $this->hasOne(Book::className(), ['id' => 'book_id']);
    }
}
