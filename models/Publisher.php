<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "publishers".
 *
 * @property int $id
 * @property string $name
 * @property string $country
 * @property int $is_active
 * @property string $date_created
 * @property string $date_updated
 *
 * @property Books[] $books
 */
class Publisher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publishers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date_created', 'date_updated'], 'required'],
            /*[['date_created', 'date_updated'], 'safe'],
            [['name', 'country'], 'string', 'max' => 45],
            [['is_active'], 'string', 'max' => 4],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'country' => 'Country',
            'is_active' => 'Is Active',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Book::className(), ['publisher_id' => 'id']);
    }
}
