<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "books".
 *
 * @property int $id
 * @property string $isbn
 * @property string $title
 * @property int $pages
 * @property string $description
 * @property string $image
 * @property int $category_id
 * @property int $publisher_id
 * @property int $is_active
 * @property string $date_created
 * @property string $date_updated
 * @property string $path
 *
 * @property BookAudiences[] $bookAudiences
 * @property Audiences[] $audiences
 * @property BookAuthors[] $bookAuthors
 * @property Authors[] $authors
 * @property Categories $category
 * @property Publishers $publisher
 */
class Book extends \yii\db\ActiveRecord
{
    public $book_image;
    public $ebook_pdf;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['isbn', 'title', 'pages', 'category_id', 'publisher_id', 'date_created', 'date_updated', 'path'], 'required'],
            /*[['pages', 'category_id', 'publisher_id'], 'integer'],
            [['description'], 'string'],
            [['date_created', 'date_updated'], 'safe'],
            [['isbn'], 'string', 'max' => 13],
            [['title', 'image', 'path'], 'string', 'max' => 255],
            [['is_active'], 'string', 'max' => 4],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['publisher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Publisher::className(), 'targetAttribute' => ['publisher_id' => 'id']],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'isbn' => 'Isbn',
            'title' => 'Title',
            'pages' => 'Pages',
            'description' => 'Description',
            'image' => 'Image',
            'category_id' => 'Category ID',
            'publisher_id' => 'Publisher ID',
            'is_active' => 'Is Active',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'path' => 'Path',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookAudiences()
    {
        return $this->hasMany(BookAudience::className(), ['book_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAudiences()
    {
        return $this->hasMany(Audience::className(), ['id' => 'audience_id'])->viaTable('book_audiences', ['book_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookAuthors()
    {
        return $this->hasMany(BookAuthors::className(), ['book_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthors()
    {
        return $this->hasMany(Author::className(), ['id' => 'author_id'])->viaTable('book_authors', ['book_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisher()
    {
        return $this->hasOne(Publisher::className(), ['id' => 'publisher_id']);
    }
}
