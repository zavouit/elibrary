<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "book_audiences".
 *
 * @property int $book_id
 * @property int $audience_id
 * @property int $is_active
 * @property string $date_created
 * @property string $date_updated
 *
 * @property Audiences $audience
 * @property Books $book
 */
class BookAudiences extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'book_audiences';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['book_id', 'audience_id','last_updated'], 'required'],
           /*[['book_id', 'audience_id'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['is_active'], 'string', 'max' => 4],
            [['book_id', 'audience_id'], 'unique', 'targetAttribute' => ['book_id', 'audience_id']],
            [['audience_id'], 'exist', 'skipOnError' => true, 'targetClass' => Audience::className(), 'targetAttribute' => ['audience_id' => 'id']],
            [['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => Book::className(), 'targetAttribute' => ['book_id' => 'id']],
        */ ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'book_id' => 'Book ID',
            'audience_id' => 'Audience ID',
            'is_active' => 'Is Active',
            'last_updated' => 'last_updated',
            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAudience()
    {
        return $this->hasOne(Audience::className(), ['id' => 'audience_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBook()
    {
        return $this->hasOne(Book::className(), ['id' => 'book_id']);
    }
}
