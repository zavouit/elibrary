<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\UploadedFile;
use app\models\Author;
use app\models\Books;
use app\models\Category;
use app\models\Publisher;

class AuthorController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $authors = Author::find()
            ->indexBy('id')
            ->all();
       
        return $this->renderPartial(
            'view',
            ['authors' => $authors]
        );
        
    }
    public function actionAdd()
    {
        $author = new Author();
        $request = Yii::$app->request;
        $author->fname = $request->post('fname','');
        $author->lname = $request->post('lname','');
        $temp_date = $request->post('date_of_birth','');
        $author->date_of_birth = date('Y-m-d',strtotime($temp_date));
        $author->gender = $request->post('gender','');
        $author->description = $request->post('description','');
       
        $author->date_created = date("Y-m-d H:i:s");
        $author->date_updated = date("Y-m-d H:i:s");
        $author->is_active = true;
        
        $author->photo = UploadedFile::getInstanceByName("fileToUpload");
        $storage_path = 'ebooks/authors/' .  $author->photo->baseName . '.' .  $author->photo->extension;
        $author->photo->saveAs($storage_path);
        $author->image = $storage_path;
        
        
        //move_uploaded_file($tmp_name, "data/$name");
        $author->save(false);
        return "1";
    }

    public function actionBooks()
    {
       
        $request = Yii::$app->request;
        $id = $request->get('id','');
        $author = Author::findOne($id);
        
        $books = $author->books;
        $categories = Category::find()
        ->indexBy('id')
        ->all();

        $authors = Author::find()
        ->indexBy('id')
        ->all();
        
        $publishers = Publisher::find()
        ->indexBy('id')
        ->all();
       
        return $this->renderPartial(
            'books',
            ['books' => $books,'authors' => $authors,'categories' => $categories,'publishers' => $publishers,
            'author' => $author]
        );
    }
  
}
