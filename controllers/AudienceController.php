<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Category;
use app\models\Book;
use app\models\Publisher;
use app\models\Author;
use app\models\Audience;

class AudienceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
       

        $categories = Category::find()
        ->indexBy('id')
        ->all();
        return $this->render('index',['categories' => $categories]);
    }

   
    public function actionBooks()
    {
       
        $request = Yii::$app->request;
        $id = $request->get('id','');
        $audience = Audience::findOne($id);
        $books = $audience->books;
        $categories = Category::find()
        ->indexBy('id')
        ->all();

        $authors = Author::find()
        ->indexBy('id')
        ->all();
        
        $publishers = Publisher::find()
        ->indexBy('id')
        ->all();
       
        return $this->renderPartial(
            'view',
            ['books' => $books,'authors' => $authors,'categories' => $categories,'publishers' => $publishers]
        );
    }
   
}
