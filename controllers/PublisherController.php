<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\ContactForm;

use app\models\Category;
use app\models\Book;
use app\models\Publisher;
use app\models\Author;

class PublisherController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $publishers = Publisher::find()
            ->indexBy('id')
            ->all();
       
        return $this->renderPartial(
            'view',
            ['publishers' => $publishers]
        );
        
    }

    public function actionAdd()
    {
        $publisher = new Publisher();
        $request = Yii::$app->request;
        $publisher->name = $request->post('name','');
        $publisher->country = $request->post('contry','');
       
       
        $publisher->date_created = date("Y-m-d H:i:s");
        $publisher->date_updated = date("Y-m-d H:i:s");
        $publisher->is_active = true;
        $publisher->save();
        return "1";
    }
    public function actionBooks()
    {
       
        $request = Yii::$app->request;
        $id = $request->get('id','');
        $publisher = Publisher::findOne($id);
        
        $books = $publisher->books;
        $categories = Category::find()
        ->indexBy('id')
        ->all();

        $authors = Author::find()
        ->indexBy('id')
        ->all();
        
        $publishers = Publisher::find()
        ->indexBy('id')
        ->all();
       
        return $this->renderPartial(
            'books',
            ['books' => $books,'authors' => $authors,'categories' => $categories,'publishers' => $publishers]
        );
    }
}
