<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use app\models\Book;
use app\models\Category;
use app\models\Author;
use app\models\Publisher;

use app\models\ContactForm;
use app\models\BookAuthors;
use app\models\Audience;
use app\models\BookAudiences;


class BookController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $books = Book::find()
            ->indexBy('id')
            ->all();

        $categories = Category::find()
            ->indexBy('id')
            ->all();

        $authors = Author::find()
            ->indexBy('id')
            ->all();

        $publishers = Publisher::find()
            ->indexBy('id')
            ->all();

        $audiences = Audience::find()
            ->indexBy('id')
            ->all();

        return $this->renderPartial(
            'view',
            ['books' => $books, 'authors' => $authors, 'categories' => $categories, 'publishers' => $publishers, 'audiences' => $audiences]
        );

    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionAdd()
    {
        $book = new Book();
        $bookAuthor = new BookAuthors();

        $request = Yii::$app->request;
        $book->title = $request->post('tittle', '');
        $book->isbn = $request->post('isbn', '');
        $temp_date = $request->post('date-published', '');
        $book->date_published = date('Y-m-d', strtotime($temp_date));
        $book->pages = $request->post('pages', '');
        $book->description = $request->post('description', '');
        $aurthor_ids = $request->post('author', '');
        $audience_ids = $request->post('audiences', '');

        $book->category_id = $request->post('category', '');
        $book->publisher_id = $request->post('publisher', '');
        $book->date_created = date("Y-m-d H:i:s");
        $book->date_updated = date("Y-m-d H:i:s");
        $book->is_active = true;

        $book->book_image = UploadedFile::getInstanceByName("book_image");
        $storage_path = 'ebooks/image/' . $book->book_image->baseName . '.' . $book->book_image->extension;
        $book->book_image->saveAs($storage_path);
        $book->image = $storage_path;

        $book->ebook_pdf = UploadedFile::getInstanceByName("ebook");
        $storage_path = 'ebooks/book/' . $book->ebook_pdf->baseName . '.' . $book->ebook_pdf->extension;
        $book->ebook_pdf->saveAs($storage_path);
        $book->path = $storage_path;
       
        
        
        //move_uploaded_file($tmp_name, "data/$name");
        $book->save();
        foreach ($aurthor_ids as $author_id) {
            $bookAuthor = new BookAuthors();
            $bookAuthor->author_id = $author_id;
            $bookAuthor->book_id = $book->id;
            $bookAuthor->last_updated = date("Y-m-d H:i:s");
            $bookAuthor->save();
        }
        foreach ($audience_ids as $audience_id) {
            $bookAudience = new BookAudiences();
            $bookAudience->audience_id = $audience_id;
            $bookAudience->book_id = $book->id;
            $bookAudience->last_updated = date("Y-m-d H:i:s");
            $bookAudience->save();
        }





        return "1";
    }
    public function actionSaveEdit()
    {
        
        $bookAuthor = new BookAuthors();

        $request = Yii::$app->request;
        $id = $request->post('id', '');
        $book = Book::findOne($id);
        $book->title = $request->post('tittle', '');
        $book->isbn = $request->post('isbn', '');
        $temp_date = $request->post('date-published', '');
        $book->date_published = date('Y-m-d', strtotime($temp_date));
        $book->pages = $request->post('pages', '');
        $book->description = $request->post('description', '');
        $aurthor_ids = $request->post('author', '');
        $audience_ids = $request->post('audiences', '');

        $book->category_id = $request->post('category', '');
        $book->publisher_id = $request->post('publisher', '');
       
        $book->date_updated = date("Y-m-d H:i:s");
       
        if (!is_null(UploadedFile::getInstanceByName("book_image"))) {
            $book->book_image = UploadedFile::getInstanceByName("book_image");
            $storage_path = 'ebooks/image/' . $book->book_image->baseName . '.' . $book->book_image->extension;
            $book->book_image->saveAs($storage_path);
            $book->image = $storage_path;
        }
        if (!is_null(UploadedFile::getInstanceByName("ebook"))) {
            $book->ebook_pdf = UploadedFile::getInstanceByName("ebook");
            $storage_path = 'ebooks/book/' . $book->ebook_pdf->baseName . '.' . $book->ebook_pdf->extension;
            $book->ebook_pdf->saveAs($storage_path);
            $book->path = $storage_path;
        }
        
        
        //move_uploaded_file($tmp_name, "data/$name");
        $book->save();
        BookAuthors::deleteAll(['book_id' => $book->id]);
        foreach ($aurthor_ids as $author_id) {
            $bookAuthor = new BookAuthors();
            $bookAuthor->author_id = $author_id;
            $bookAuthor->book_id = $book->id;
            $bookAuthor->last_updated = date("Y-m-d H:i:s");
           
            $bookAuthor->save();
        }
        BookAudiences::deleteAll(['book_id' => $book->id]);
        foreach ($audience_ids as $audience_id) {
            $bookAudience = new BookAudiences();
            $bookAudience->audience_id = $audience_id;
            $bookAudience->book_id = $book->id;
            $bookAudience->last_updated = date("Y-m-d H:i:s");
            $bookAudience->save();
        }

        $books = Book::find()
            ->indexBy('id')
            ->all();

        $categories = Category::find()
            ->indexBy('id')
            ->all();

        $authors = Author::find()
            ->indexBy('id')
            ->all();

        $publishers = Publisher::find()
            ->indexBy('id')
            ->all();

        $audiences = Audience::find()
            ->indexBy('id')
            ->all();

        return $this->renderPartial(
            'view',
            ['books' => $books, 'authors' => $authors, 'categories' => $categories, 'publishers' => $publishers, 'audiences' => $audiences]
        );

    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionMenu()
    {
        $categories = Category::find()
            ->indexBy('id')
            ->all();
        $audiences = Audience::find()
            ->indexBy('id')
            ->all();
        return $this->renderPartial('menu', ['categories' => $categories, 'audiences' => $audiences]);

    }
    public function actionAudienceMenu()
    {

        $audiences = Audience::find()
            ->indexBy('id')
            ->all();
        return $this->renderPartial('menuaud', ['audiences' => $audiences]);

    }
    public function actionDetails()
    {

        $request = Yii::$app->request;
        $id = $request->get('id', '');
        $book = Book::findOne($id);
        return $this->renderPartial('details', ['book' => $book]);

    }
    public function actionDelete()
    {

        $request = Yii::$app->request;
        $id = $request->get('id', '');
        $book = Book::findOne($id);
        BookAuthors::deleteAll(['book_id' => $book->id]);        
        BookAudiences::deleteAll(['book_id' => $book->id]);
        $book->delete();
        $books = Book::find()
            ->indexBy('id')
            ->all();

        $categories = Category::find()
            ->indexBy('id')
            ->all();

        $authors = Author::find()
            ->indexBy('id')
            ->all();

        $publishers = Publisher::find()
            ->indexBy('id')
            ->all();

        $audiences = Audience::find()
            ->indexBy('id')
            ->all();

        return $this->renderPartial(
            'view',
            ['books' => $books, 'authors' => $authors, 'categories' => $categories, 'publishers' => $publishers, 'audiences' => $audiences]
        );

    }
public function actionEdit()
    {
        $books = Book::find()
            ->indexBy('id')
            ->all();

        $categories = Category::find()
            ->indexBy('id')
            ->all();

        $authors = Author::find()
            ->indexBy('id')
            ->all();

        $publishers = Publisher::find()
            ->indexBy('id')
            ->all();

        $audiences = Audience::find()
            ->indexBy('id')
            ->all();
        $request = Yii::$app->request;
        $id = $request->get('id', '');
        $book = Book::findOne($id);
        return $this->renderPartial('edit', ['book' => $book, 'authors' => $authors, 'categories' => $categories, 'publishers' => $publishers, 'audiences' => $audiences]);

    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */


    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
