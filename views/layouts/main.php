<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Fiji eLibrary</title>
    <!-- Basic Styles -->
	<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" media="screen" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" media="screen" href="js/DataTables/datatables.css">
	<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-datepicker.css">
	<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
	<link rel="stylesheet" type="text/css" media="screen" href="css/selectize.css">
	<link rel="stylesheet" href="css/normalize.css">
		<link rel="stylesheet" href="css/stylesheet.css">

	<!-- SmartAdmin Styles : Caution! DO NOT change the order -->

	<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-production.min.css">
</head>
<body class="">
	<!-- HEADER -->
	<header id="header">
		<div>
			
				<img  id="zavouit-logo" width="85%" src="img/Zavouit_Logo.png" height="95%" alt="ZAVOU IT">
			
		</div>
		<div>
		<h2> Fiji eLibrary</h2>
	    </div>

	</header>
	<!-- END HEADER -->

	<!-- Left panel : Navigation area -->
	<!-- Note: This width of the aside area can be adjusted through LESS variables -->
	<aside id="left-panel">

		<!-- User info -->
		<div class="login-info">
			<span>
				
				

			</span>
		</div>
		<!-- end user info -->

		<!-- NAVIGATION : This navigation is also responsive-->
		<nav>
			<!-- 
				NOTE: Notice the gaps after each icon usage <i></i>..
				Please note that these links work a bit different than
				traditional href="" links. See documentation for details.
				-->

			<ul>
				<li>
					<a href="book/index" onclick="doalert(this); return false;"><i class="fa fa-lg fa-fw fa-book"></i> <span class="menu-item-parent">Books</span></a>
					<!--ul>
						<li class="">
							<a href="book/index" onclick="doalert(this); return false;" title="Dashboard"><i class="fa fa-lg fa-fw fa-file"></i><span
								 class="menu-item-parent">All Books</span></a>
						</li>
						

					</ul -->
				</li>
				<li>
					<a href="#" title="Audience"><i class="fa fa-lg fa-fw fa-users"></i> <span class="menu-item-parent">Audience</span></a>
					<ul id="aud">
						
					

					</ul>
				</li>
				<li class="top-menu-invisible">
					<a href="author/index" onclick="doalert(this); return false;" title="Dashboard"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i> <span class="menu-item-parent">Authors</span></a>
					
				</li>

				<li class="top-menu-invisible">
					<a href="publisher/index" onclick="doalert(this); return false;" title="Dashboard"><i class="fa fa-lg fa-fw fa-wrench"></i> <span class="menu-item-parent">Publications</span></a>
					
				</li>
				<li>
					<a href="#" title="Dashboard"><i class="fa fa-lg fa-fw fa-tags"></i> <span class="menu-item-parent">Categories</span></a>
					<ul id="dewi">
						
					

					</ul>
				</li>
			</ul>
		</nav>


		<span class="minifyme" data-action="minifyMenu">
			<i class="fa fa-arrow-circle-left hit"></i>
		</span>

	</aside>
	<!-- END NAVIGATION -->

	<!-- MAIN PANEL -->
	<div id="main" role="main">

		<!-- RIBBON -->
		<div id="ribbon">

			<span class="ribbon-button-alignment">
				<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip"
				 data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings."
				 data-html="true">
					<i class="fa fa-refresh"></i>
				</span>
			</span>

			<!-- breadcrumb -->
			<ol class="breadcrumb">
				<li>Home</li>
				<li>Dashboard</li>
			</ol>
			<!-- end breadcrumb -->

			<!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right">
				<span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
				<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
				<span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
				</span> -->

		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">

			<!--ENTER MAIN CONTENT-->



		</div>
		<!-- END MAIN CONTENT -->
		<div id='crud_modal'>
		</div>
	</div>
	<!-- END MAIN PANEL -->

	<!-- PAGE FOOTER -->
	<div class="page-footer">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<span class="txt-color-white">ZAVOU IT ©
					2018
			</div>

			<div class="col-xs-6 col-sm-6 text-right hidden-xs">
				
			</div>
		</div>
	</div>
	<!-- END PAGE FOOTER -->

	<!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
		Note: These tiles are completely responsive,
		you can add as many as you like
		-->
	<div id="shortcut">
		<ul>
			<li>
				<a href="inbox.html" class="jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i>
						<span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>
			</li>
			<li>
				<a href="calendar.html" class="jarvismetro-tile big-cubes bg-color-orangeDark"> <span class="iconbox"> <i class="fa fa-calendar fa-4x"></i>
						<span>Calendar</span> </span> </a>
			</li>
			<li>
				<a href="gmap-xml.html" class="jarvismetro-tile big-cubes bg-color-purple"> <span class="iconbox"> <i class="fa fa-map-marker fa-4x"></i>
						<span>Maps</span> </span> </a>
			</li>
			<li>
				<a href="invoice.html" class="jarvismetro-tile big-cubes bg-color-blueDark"> <span class="iconbox"> <i class="fa fa-book fa-4x"></i>
						<span>Invoice <span class="label pull-right bg-color-darken">99</span></span> </span> </a>
			</li>
			<li>
				<a href="gallery.html" class="jarvismetro-tile big-cubes bg-color-greenLight"> <span class="iconbox"> <i class="fa fa-picture-o fa-4x"></i>
						<span>Gallery </span> </span> </a>
			</li>
			<li>
				<a href="profile.html" class="jarvismetro-tile big-cubes selected bg-color-pinkDark"> <span class="iconbox"> <i
						 class="fa fa-user fa-4x"></i> <span>My Profile </span> </span> </a>
			</li>
		</ul>
	</div>
	
	<!-- END SHORTCUT AREA -->

	<!--================================================== -->


	<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->

	<script src="js/libs/jquery-3.2.1.min.js"></script>
	<script src="js/libs/bootstrap.js"></script>

	<!-- IMPORTANT: APP CONFIG -->
	<script src="js/app.config.js"></script>

	<script src="js/DataTables/datatables.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/selectize.js"></script>

	<!-- MAIN APP JS FILE -->
	<script src="js/app.min.js"></script>
	<script>
		function doalert(obj) {
			url = obj.getAttribute("href")
            //mainUrl = "http://localhost/secondary/web/site/index"
            //alert(url);
			$.get(url, function (data, status) {
				jQuery("#content").html(data);
				$('#test_papers').DataTable();
				$('.single').selectize({
								create: true,
								sortField: {
									field: 'text',
									direction: 'asc'
								},
								//maxItems:10
								//dropdownParent: 'body'
							});
				
			});
			return false;

		}

		function getDetails(obj) {
			url = obj.getAttribute("href")
            //mainUrl = "http://localhost/secondary/web/site/index"
            //alert(url);
			$.get(url, function (data, status) {
				jQuery("#crud_modal").html(data);
				$('#modal_details').modal('show');
				
				
			});
			return false;

		}
		function getEdit(obj) {
			url = obj.getAttribute("href")
            //mainUrl = "http://localhost/secondary/web/site/index"
            //alert(url);
			$.get(url, function (data, status) {
				jQuery("#crud_modal").html(data);
				$('#edit_modal').modal('show');
				$('.single').selectize({
								create: true,
								sortField: {
									field: 'text',
									direction: 'asc'
								},
								//maxItems:10
								//dropdownParent: 'body'
							});
				
			});
			return false;

		}
		
		$("li").click(function () {
			$(this).toggleClass("active");
			$(this).siblings().removeClass("active");
		});
		$( function() {
    		$( "#datepicker" ).datepicker();
  		} );

		$( document ).ready(function() {
			$.get('book/menu', function (data, status) {
				//alert("ons");
				jQuery("#dewi").html(data);
				
			});
			$.get('book/audience-menu', function (data, status) {
				//alert("ons");
				jQuery("#aud").html(data);
				
			});
			$(document).on("click", "#add-publisher", function(event){		
				var publisher = $("#name").val();
				var country = $('#country').val();			
				var form_data = new FormData($('form')[0]);
				requestData("POST","publisher/add","#publisher-modal",form_data,"publisher/index");
				/*$.ajax({
					type: 'POST',
					url: 'publisher/add',
					data: form_data,
					processData: false,
					contentType: false,
					success: function() {
						$.get("publisher/index", function (data, status) {
							jQuery("#content").html(data);
							$('#test_papers').DataTable();
							$('select').selectize({
								create: true,
								sortField: {
									field: 'text',
									direction: 'asc'
								},
								//dropdownParent: 'body'
								maxItems:10
							});
						});
					}
				});*/
			});
			$(document).on("click", "#add-author", function(event){		
				
				var fname = $("#fname").val();
				var lname = $('#lname').val();
				var gender = $("#gender").val();
				var date_of_birth = $('#date-of-birth').val();
				var fileToUpload = '';//$("#fileToUpload").files[0];
				var description = $('#description').val();
				
				
				
				var form_data = new FormData($('form')[0]);
				requestData("POST","author/add","#author-modal",form_data,"author/index");
				
			});	
			$(document).on("click", "#save-book", function(event){		
				var isbn = $("#isbn").val();
				var tittle = $('#tittle').val();
				var pages = $("#pages").val();
				var date_published = $('#date-published').val();
				var category = $("#category").val();
				var author = $('#author').val();
				var publisher = $("#publisher").val();
				var description = $('#description').val();
				//var fileToUpload = $("#fileToUpload").files[0];

				var form = document.querySelector('#add-book');
  				var form_data = new FormData(form);
				requestData("POST","book/add","#book-modal",form_data,"book/index");

				
			});
			$(document).on("click", "#edit-book-btn", function(event){		
				var isbn = $("#isbn").val();
				var tittle = $('#tittle').val();
				var pages = $("#pages").val();
				var date_published = $('#date-published').val();
				var category = $("#category").val();
				var author = $('#author').val();
				var publisher = $("#publisher").val();
				var description = $('#description').val();
				//var fileToUpload = $("#fileToUpload").files[0];

				var form = document.querySelector('#edit-book');
  				var formData = new FormData(form);

				requestData("POST","book/save-edit","#edit_modal",formData,"book/index");

				
			});
		
		});

		function requestData(requestType,url,modal_name,form_data,refresh_results_url){
			$(modal_name).modal('hide');
				$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
				$.ajax({
					type: requestType,
					url: url,
					data: form_data,
					processData: false,
					contentType: false,
					success: function(result) {
						
						$.get(refresh_results_url, function (data, status) {
							jQuery("#content").html(data);
							$('#test_papers').DataTable();
							$('.single').selectize({
								create: true,
								sortField: {
									field: 'text',
									direction: 'asc'
								},
								//maxItems:10
								//dropdownParent: 'body'
							});
							/*$('.multi').each(function () {
								$(this).selectize({
										create: true,
										sortField: {
										field: 'text',
										direction: 'asc'
									},
									maxItems:10
								});
							});*/
						});
					}
				});
		}

	</script>
    

</body>

</html>

