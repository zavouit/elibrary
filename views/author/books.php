
    
    <?php
    use app\models\Book;
    use app\models\Category;
    use app\models\Publisher;
    use app\models\Author;
    use yii\helpers\Html;

    ?>

    <div class="card">
    <img class="card-img-top" src=<?= $author->image?> alt="Card image" height="130" weight="130">
      <div class="card-body">
        Name:<?= $author->fname?> <?= $author->lname ?><br>
        Date of Birth:<?= $author->date_of_birth ?><br>
        Gender:<?= $author->gender ?><br>
        <p class="card-text"><?= $author->description ?></p>
        
      </div>
    </div>
   
<br>        
<?php



echo "<table id='test_papers' class='stripe' style='width:100%'>
			<thead>
				<tr>
					<th>Tittle</th>
					<th>Pages</th>
                    <th>Category</th>
                    <th>Author</th>
                    <th>Publisher</th>
                    <th>Link</th>
                    <th>Action</th>
				</tr>
			</thead>
			<tbody>";
        //loop through the exam years

foreach ($books as $book) {




  echo "<tr>";
            //echo "<td>$book->isbn</td>";
  echo "<td>$book->title</td>";
  echo "<td>$book->pages</td>";

  $category = $book->category->name;
  echo "<td>$category</td>";

  echo "<td>";
  $count = 1;
  foreach ($book->authors as $author) {
    echo "$count. $author->fname $author->lname";
    echo "<br>";
    $count++;
  }
  echo "</td>";
  $publisher = $book->publisher->name;
  echo "<td>$publisher</td>";
  echo "</td>";
  echo "<td><a href='$book->path' target='_blank'>view book</a></td>";
  echo "<td><a class='btn btn-labeled btn-info'>
  View
</a>
<a class='btn btn-labeled btn-success'>
 Edit
</a>
<a class='btn btn-labeled btn-danger'>
Delete
</a></td>";
  echo "</tr>";
}
echo "</tbody>

        </table>";

    
    
       //var_dump($model->authors);
?>
<div id="book-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Book</h4>
      </div>
      <div class="modal-body">
      <form id="add-book">
  <div class="form-group">
    <label>Tittle</label>
    <input type="email" class="form-control" id="tittle" name="tittle">
  </div>
  <div class="form-group">
    <label>Upload Book Image</label>
    <input type="file" name="book_image" id="book_image" class="form-control">
  </div>
  <div class="form-group">
    <label>Ubload eBook</label>
    <input type="file" name="ebook" id="ebook" class="form-control">
  </div>

  <div class="form-group">
    <label>ISBN</label>
    <input type="text" class="form-control" id="isbn" name="isbn">
  </div>
  <div class="form-group">
    <label>Pages</label>
    <input type="number" class="form-control" id="pages" name="pages">
  </div>
  <div class="form-group">
  <label>Date Published </label>
<div class="input-group date" data-provide="datepicker">

    <input type="text" class="form-control" name="date-published">
    <div class="input-group-addon">
        <span class="glyphicon glyphicon-th"></span>
    </div>
</div>
</div>
  <div class="control-group">
    <label>Category</label>
    
    <select class="demo-default single" placeholder ="Please Select a Category" name="category">
    <option value="">Please Select a Category...</option>
    <?php
    foreach ($categories as $category) {
      echo "<option value='$category->id'>Class $category->dewi_decimal - $category->name</option>";
    }
    ?>
      </select>
      
  </div>
  <div class="control-group">
    <label>Author</label>
    <select class="demo-default " placeholder="Please Select a Author" name="author[]" id="book-author">
    <option value="">Please Select a Author...</option>
         <?php
        foreach ($authors as $author) {
          echo "<option value='$author->id'>$author->fname $author->lname</option>";
        }
        ?>
      </select>
  </div>
  <div class="control-group">
  <label>Pulisher</label>  
  <select class="demo-default single" placeholder="Please Select a Publisher" name="publisher">
  <option value="">Please Select a Publisher...</option>
    <?php
    foreach ($publishers as $publisher) {
      echo "<option value='$publisher->id'>$publisher->name</option>";
    }
    ?>
    </select>
  </div>
  <div class="form-group">
    <label>Description</label>
    <textarea class="form-control" id="description" rows="5" name="description"></textarea>
  </div>
  <input id="form-token" type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
           value="<?= Yii::$app->request->csrfToken ?>"/>

</form>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-info btn-default" id="save-book">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
