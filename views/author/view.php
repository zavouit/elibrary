
    <?php
    use app\models\Book;
    use app\models\Category;
    use app\models\Publisher;
    use app\models\Author;
    use yii\helpers\Html;

    ?>
        <p><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#author-modal">Add Authors</button></p>
            
<?php



echo "<table id='test_papers' class='stripe' style='width:100%'>
			<thead>
				<tr>
					<th>Name</th>
					<th>Date of Birth</th>
                    <th>Gender</th>
                    
				</tr>
			</thead>
			<tbody>";
        //loop through the exam years

foreach ($authors as $author) {




  echo "<tr>";
  echo "<td><a href='author/books?id=$author->id'  onclick='doalert(this); return false;'>$author->fname $author->lname</a></td>";

  echo "<td>$author->date_of_birth</td>";

  echo "<td>$author->gender</td>";
  echo "</tr>";
}
echo "</tbody>

        </table>";

    
    
       //var_dump($model->authors);
?>
<div id="author-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Author</h4>
      </div>
      <div class="modal-body">
      <form id="add-author-form">
  <div class="form-group">
    <label>First Name</label>
    <input type="text" class="form-control" id="fname" name="fname">
  </div>
  <div class="form-group">
    <label>Last Name</label>
    <input type="text" class="form-control" id="fname" name="lname">
  </div>
  <div class="form-group">
  <lable>Gender</lable>
  <div class="checkbox">
  <label><input type="checkbox" value="Male" name="gender">Male</label>
</div>
<div class="checkbox">
  <label><input type="checkbox" value="Female" name="gender">Female</label>
</div>
</div>
<div class="form-group">
<lable>Date of Birth</lable>
<div class="input-group date" data-provide="datepicker">
    <input type="text" class="form-control" name="date-of-birth">
    <div class="input-group-addon">
        <span class="glyphicon glyphicon-th"></span>
    </div>
</div>
<div class="form-group">
    <label>Image</label>
    <input type="file" name="fileToUpload" id="fileToUpload" class="form-control">
</div>
<div class="form-group">
    <label>Description</label>
    <textarea class="form-control" id="description" rows="5" name="description"></textarea>
  </div>
  <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
           value="<?=Yii::$app->request->csrfToken?>"/>
</form>
      </div>
      <div class="modal-footer">
      <button type="button" id="add-author" class="btn btn-info btn-default">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
