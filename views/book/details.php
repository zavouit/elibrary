<div id="modal_details" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Book Details</h4>
      </div>
      <div class="modal-body">     
        <div class="contatainer">
          <div class="row">
            <div class="col-sm-4">
              
                <div class="row">
                  <div class="col-sm-12">
                    <img class="card-img-top" src=<?= $book->image ?> alt="Card image" height="130" weight="130">
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <strong>Title:</strong><?= $book->title ?>
                  </div>           
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <strong>Date Published:</strong><?= date('F j, Y', strtotime($book->date_published)) ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">          
                    <strong>ISBN:</strong><?= $book->isbn ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">          
                    <strong>Pages:</strong><?= $book->pages ?>
                  </div>            
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <strong>Category:</strong>
                    <?= $book->category->name ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <strong>Author:</strong>
                    <ol>
                      <?php
                        foreach ($book->authors as $author) {
                          echo "<li>$author->fname $author->lname</li>";
                        }
                      ?>
                    </ol>
                  </div>
                </div>	  
                <div class="row">
                  <div class="col-sm-12">
                    <strong>Audience:</strong>    
                    <ol>
                    <?php
                      foreach ($book->audiences as $audience) {
                        echo "<li>$audience->name</li>";
                      }
                    ?>
                    </ol>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">        
                    <strong>Pulisher:</strong><?= $book->publisher->name ?>
                  </div>
                </div>
              
            </div>
            <div class="col-sm-8">
              
                <div class="row">
                  <div class="col-sm-12">
                    <strong>Description:</strong>
                    <p><?= $book->description ?></p>
                  </div>
                </div>
              
            </div>
          </div>
         
          
         
         
        </div>
      </div>
      <div class="modal-footer">
        <a href="<?= $book->path ?>" class="btn btn-info" role="button" download>Download Book</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
