
    <?php
    use app\models\Book;
    use app\models\Category;
    use app\models\Publisher;
    use app\models\Author;
    use yii\helpers\Html;

   ?>
        <p><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#book-modal">Add Books</button></p>
            
<?php
       

    
        echo "<table id='test_papers' class='stripe' style='width:100%'>
			<thead>
				<tr>
					<th>Tittle</th>
					<th>Pages</th>
                    <th>Category</th>
                    <th>Audience</th>
                    <th>Author</th>
                    <th>Publisher</th>
                    
                    
                    <th>Action</th>
				</tr>
			</thead>
			<tbody>";
        //loop through the exam years

        foreach ($books as $book) {




            echo "<tr>";
            //echo "<td>$book->isbn</td>";
            echo "<td>$book->title</td>";
            echo "<td>$book->pages</td>";

            $category = $book->category->name;
            echo "<td>$category</td>";
            echo "<td>";
            $count = 1;
            foreach ($book->audiences as $audience) {
                echo "$count. $audience->name";
                echo "<br>";
                $count++;
            }
            
            echo "</td>";
            echo "<td>";
            $count = 1;
            foreach ($book->authors as $author) {
                echo "$count. $author->fname $author->lname";
                echo "<br>";
                $count++;
            }

            echo "</td>";
            $publisher = $book->publisher->name;
            echo "<td>$publisher</td>";
            echo "</td>";
           
            echo "<td><a href='book/details?id=$book->id' id='btn_details' class='btn btn-labeled btn-info'onclick='getDetails(this); return false;'>
  Details
</a>
<a href='book/edit?id=$book->id' id='btn_edit' class='btn btn-labeled btn-success' onclick='getEdit(this); return false;'>
 Edit
</a>
<a href='book/delete?id=$book->id' id='btn_delete' class='btn btn-labeled btn-danger' onclick='doalert(this); return false;'>
Delete
</a></td>";
            echo "</tr>";
        }
        echo "</tbody>

        </table>";

    
    
       //var_dump($model->authors);
    ?>
<div id="book-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Book</h4>
      </div>
      <div class="modal-body">
      <form id="add-book">
  <div class="form-group">
    <label>Tittle</label>
    <input type="email" class="form-control" id="tittle" name="tittle">
  </div>
  <div class="form-group">
    <label>Upload Book Image</label>
    <input type="file" name="book_image" id="book_image" class="form-control">
  </div>
  <div class="form-group">
    <label>Ubload eBook</label>
    <input type="file" name="ebook" id="ebook" class="form-control">
  </div>

  <div class="form-group">
    <label>ISBN</label>
    <input type="text" class="form-control" id="isbn" name="isbn">
  </div>
  <div class="form-group">
    <label>Pages</label>
    <input type="number" class="form-control" id="pages" name="pages">
  </div>
  <div class="form-group">
    <label>Date Published </label>
    <div class="input-group date" data-provide="datepicker">
    <input type="text" class="form-control" name="date-published">
    <div class="input-group-addon">
      <span class="glyphicon glyphicon-th"></span>
    </div>
  </div>


  <div class="control-group">
    <label>Audience</label>    
    <select class="demo-default multi" placeholder ="Please Select a Audience" name="audiences[]" id="book-audience">
    <option value="">Please Select a Audience...</option>
    <?php
      foreach($audiences as $audience){
        echo "<option value='$audience->id'>$audience->name</option>";
      }
    ?>
    </select>
    <script>
				$('#book-audience').selectize({
					maxItems: 10
				});
		</script>
			
  </div>

  <div class="control-group">
    <label>Category</label>
    
    <select class="demo-default single" placeholder ="Please Select a Category" name="category">
    <option value="">Please Select a Category...</option>
    <?php
      foreach($categories as $category){
        echo "<option value='$category->id'>Class $category->dewi_decimal - $category->name</option>";
      }
    ?>
      </select>
      
  </div>
  <div class="control-group">
    <label>Author</label>
    <select class="demo-default multi" placeholder="Please Select a Author" name="author[]" id="book-author">
    <option value="">Please Select a Author...</option>
         <?php
      foreach($authors as $author){
        echo "<option value='$author->id'>$author->fname $author->lname</option>";
      }
    ?>
      </select>
      <script>
				$('#book-author').selectize({
					maxItems: 10
				});
				</script>
			
  </div>
  <div class="control-group">
  <label>Pulisher</label>  
  <select class="demo-default single" placeholder="Please Select a Publisher" name="publisher">
  <option value="">Please Select a Publisher...</option>
    <?php
    foreach($publishers as $publisher){
      echo "<option value='$publisher->id'>$publisher->name</option>";
      }
      ?>
    </select>
  </div>
  <div class="form-group">
    <label>Description</label>
    <textarea class="form-control" id="description" rows="5" name="description"></textarea>
  </div>
  <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
           value="<?=Yii::$app->request->csrfToken?>"/>

</form>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-info btn-default" id="save-book">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
