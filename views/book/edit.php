<div id="edit_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Book</h4>
      </div>
      <div class="modal-body">
      <form id="edit-book">
  <div class="form-group">
    <label>Title</label>
    <input type="email" class="form-control" id="tittle" name="tittle" value="<?= $book->title ?>">
  </div>
  <div class="form-group">
    <label>Change Book Image</label>
    <input type="file" name="book_image" id="book_image" class="form-control" value="<?= $book->image ?>">
  </div>
  <div class="form-group">
    <label>Change eBook</label>
    <input type="file" name="ebook" id="ebook" class="form-control" value="<?= $book->path ?>">
  </div>

  <div class="form-group">
    <label>ISBN</label>
    <input type="text" class="form-control" id="isbn" name="isbn" value="<?= $book->isbn ?>">
  </div>
  <div class="form-group">
    <label>Pages</label>
    <input type="number" class="form-control" id="pages" name="pages" value="<?= $book->pages ?>">
  </div>
  <div class="form-group">
    <label>Date Published </label>
    <div class="input-group date" data-provide="datepicker">
    <input type="text" class="form-control" name="date-published" value="<?= $book->date_published ?>">
    <div class="input-group-addon">
      <span class="glyphicon glyphicon-th"></span>
    </div>
  </div>


  <div class="control-group">
    <label>Audience</label>    
    <select multiple class="demo-default multi" placeholder ="Please Select a Audience" name="audiences[]" id="edit-audience">
    <option value="">Please Select a Audience...</option>
    <?php
      foreach($audiences as $audience){
          $selected = "";
        foreach($book->audiences as $book_audience)
        {
            if($audience->id == $book_audience->id)
            {
                $selected = "selected";
                break;
            }
        }
        echo "<option $selected value='$audience->id'>$audience->name</option>";
      }
    ?>
    </select>
    <script>
				$('#edit-audience').selectize({
					maxItems: 10
				});
	</script>
			
  </div>

  <div class="control-group">
    <label>Category</label>
    
    <select class="demo-default single" placeholder ="Please Select a Category" name="category">
    <option value="">Please Select a Category...</option>
    <?php
        
      foreach($categories as $category){
        $selected ="";
       
           if($book->category->id == $category->id) 
           {
            $selected = "selected";
           }
      
      echo "<option $selected value='$category->id' $selected>Class $category->dewi_decimal - $category->name</option>";
    }
    ?>
      </select>
      
  </div>
  <div class="control-group">
    <label>Author</label>
    <select multiple class="demo-default multi" placeholder="Please Select a Author" name="author[]" id="edit-author">
    <option value="">Please Select a Author...</option>
         <?php
      foreach($authors as $author){
          $selected = "";
          foreach($book->authors as $book_author)
          {
            if($book_author->id == $author->id)
            {
                $selected = "selected";
                break;
            }
          }
        echo "<option $selected value='$author->id'>$author->fname $author->lname</option>";
      }
    ?>
      </select>
      <script>
				$('#edit-author').selectize({
					maxItems: 10
				});
	  </script>
			
  </div>
  <div class="control-group">
  <label>Pulisher</label>  
  <select class="demo-default single" placeholder="Please Select a Publisher" name="publisher">
  <option value="">Please Select a Publisher...</option>
    <?php
    foreach($publishers as $publisher){
        $selected="";
        if($book->publisher->id == $publisher->id)
        {
            $selected = "selected";
        }
      echo "<option $selected value='$publisher->id'>$publisher->name</option>";
      }
      ?>
    </select>
  </div>
  <div class="form-group">
    <label>Description</label>
    <textarea class="form-control" id="description" rows="5" name="description"><?= $book->description ?></textarea>
  </div>
  <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
           value="<?=Yii::$app->request->csrfToken?>"/>
           <input id="id" type="hidden" name="id"
           value="<?=$book->id ?>"/>

</form>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-info btn-default" id="edit-book-btn">Save Changes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
