-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 12, 2019 at 06:58 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `library`
--

-- --------------------------------------------------------

--
-- Table structure for table `audiences`
--

CREATE TABLE `audiences` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` text,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `audiences`
--

INSERT INTO `audiences` (`id`, `name`, `description`, `is_active`, `date_created`, `date_updated`) VALUES
(1, 'Preschool Studenst', 'sa', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Prmary School Students', NULL, 1, '2018-12-06 00:00:00', '2018-12-06 00:00:00'),
(3, 'Secondary School Students', NULL, 1, '2018-12-06 00:00:00', '2018-12-06 00:00:00'),
(4, 'Adults', NULL, 1, '2018-12-06 00:00:00', '2018-12-06 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `id` int(11) NOT NULL,
  `lname` varchar(45) NOT NULL,
  `fname` varchar(45) NOT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` text,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`id`, `lname`, `fname`, `gender`, `date_of_birth`, `image`, `description`, `is_active`, `date_created`, `date_updated`) VALUES
(1, 'Julian', 'Jerry', 'Male', '1976-05-03', 'sa', 'sa', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'koroi', 'rusi', 'Male', '1970-01-01', 'ebooks/authors/1011245_orig.jpg', 'test', 1, '2018-11-28 00:21:23', '2018-11-28 00:21:23'),
(3, 'Seniwaitui', 'DOmeniko', 'Male', '1970-01-01', 'ebooks/authors/1011245_orig.jpg', 'test', 1, '2018-11-28 00:24:03', '2018-11-28 00:24:03'),
(4, 'Nails', 'Tom Thumb', 'Male', '1970-01-01', 'ebooks/authors/6768666-1080p-wallpapers.jpg', 'tesee', 1, '2018-12-04 04:13:16', '2018-12-04 04:13:16');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `isbn` varchar(13) NOT NULL,
  `title` varchar(255) NOT NULL,
  `pages` int(11) NOT NULL,
  `description` text,
  `category_id` int(11) NOT NULL,
  `publisher_id` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `path` varchar(255) NOT NULL,
  `date_published` date NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `isbn`, `title`, `pages`, `description`, `category_id`, `publisher_id`, `is_active`, `date_created`, `date_updated`, `path`, `date_published`, `image`) VALUES
(1, '123456789', 'sa', 12, 'sa', 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00', NULL),
(2, '23321', 'sdsaa', 12, 'sadadaaaaa', 1, 1, 1, '2018-11-12 09:46:25', '2018-11-12 09:46:25', 'ebooks/1011245_orig.jpg', '2018-11-01', NULL),
(3, '12332', 'test', 123, 'aeewre', 1, 1, 1, '2018-11-12 10:02:34', '2018-11-12 10:02:34', 'ebooks/1011245_orig.jpg', '2018-11-03', NULL),
(5, '2321312312', 'test', 123, 'this is it', 1, 1, 1, '2018-11-12 19:22:26', '2018-11-12 19:22:26', 'ebooks/book/1011245_orig.jpg', '2018-11-01', 'ebooks/image/1011245_orig.jpg'),
(7, '2312', 'this should be it', 1234, 'test', 1, 1, 1, '2018-11-12 19:32:12', '2018-11-12 19:32:12', 'ebooks/book/79438-blue-world-map_nJEOoUQ.jpg', '2018-11-01', 'ebooks/image/6768666-1080p-wallpapers.jpg'),
(8, '2312', 'this should be it', 1234, 'test', 1, 1, 1, '2018-11-12 19:32:15', '2018-11-12 19:32:15', 'ebooks/book/79438-blue-world-map_nJEOoUQ.jpg', '2018-11-01', 'ebooks/image/6768666-1080p-wallpapers.jpg'),
(9, '1', 'test', 1, 'set kapadido', 1, 1, 1, '2018-11-12 19:33:55', '2018-11-12 19:33:55', 'ebooks/book/Puppy_Fixed.jpg', '2018-11-03', 'ebooks/image/test.jpg'),
(11, '2345', 'test 23', 23, '1233456', 1, 1, 1, '2018-11-12 19:59:16', '2018-11-12 19:59:16', 'ebooks/book/Puppy_Smaller.jpg', '2018-11-16', 'ebooks/image/Puppy_Fixed.jpg'),
(12, '2345', 'test 24', 234, 'test', 1, 1, 1, '2018-11-12 20:03:57', '2018-11-12 20:03:57', 'ebooks/book/test.jpg', '2018-11-02', 'ebooks/image/xperia_z_ultra-wallpaper-1366x768.jpg'),
(13, '1', 'q', 1, '1', 1, 1, 1, '2018-11-12 20:06:02', '2018-11-12 20:06:02', 'ebooks/book/test.jpg', '2018-11-09', 'ebooks/image/xperia_z_ultra-wallpaper-1366x768.jpg'),
(14, '2', 'thi is new', 2, 'test', 1, 1, 1, '2018-11-12 20:08:11', '2019-01-04 03:30:01', 'ebooks/book/test.jpg', '2018-11-30', 'ebooks/image/xperia_z_ultra-wallpaper-1366x768.jpg'),
(15, '3242342342', 'Test', 23, 'sdfsdfsdf', 1, 1, 1, '2018-11-27 01:29:47', '2018-11-27 01:29:47', 'ebooks/book/1011245_orig.jpg', '2018-11-15', 'ebooks/image/1011245_orig.jpg'),
(16, '2137210737812', 'Thats Ims', 23, 'test', 1, 1, 1, '2018-11-28 02:49:07', '2018-11-28 02:49:07', 'ebooks/book/1011245_orig.jpg', '2018-11-20', 'ebooks/image/1011245_orig.jpg'),
(17, '213123124321', 'ones', 23, 'wqeqweqweqw', 1, 1, 1, '2018-11-28 03:02:40', '2018-11-28 03:02:40', 'ebooks/book/1011245_orig.jpg', '2018-11-03', 'ebooks/image/1011245_orig.jpg'),
(18, '12345', 'test 123', 123, 'test', 1, 1, 1, '2018-11-28 03:16:07', '2018-11-28 03:16:07', 'ebooks/book/1011245_orig.jpg', '2018-11-14', 'ebooks/image/1011245_orig.jpg'),
(19, '123', 'test', 234, 'sdasda', 1, 2, 1, '2018-11-28 03:21:37', '2018-11-28 03:21:37', 'ebooks/book/1011245_orig.jpg', '2018-11-28', 'ebooks/image/1011245_orig.jpg'),
(20, '123454455', 'Tes Tukana', 123, 'this is it', 1, 1, 1, '2018-11-28 03:29:07', '2018-11-28 03:29:07', 'ebooks/book/1011245_orig.jpg', '2018-11-16', 'ebooks/image/1011245_orig.jpg'),
(21, '12345', 'test', 12, 'test', 3, 2, 1, '2018-12-04 04:15:08', '2018-12-04 04:15:08', 'ebooks/book/1011245_orig.jpg', '2018-12-01', 'ebooks/image/1011245_orig.jpg'),
(26, '980990', 'test audience', 123, 'eses', 6, 2, 1, '2018-12-06 06:39:59', '2018-12-06 06:39:59', 'ebooks/book/1011245_orig.jpg', '2018-12-15', 'ebooks/image/1011245_orig.jpg'),
(27, '2131312222', 'Jack n Jill', 123, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus molestie, risus non cursus ullamcorper, urna quam sodales ex, vel suscipit justo erat ac mi. Nullam nisl ante, elementum ut euismod in, blandit ut lectus. Proin at sodales dolor. Pellentesque vel tortor nec turpis sodales fermentum. Nulla imperdiet nisi quam, eget mollis quam eleifend vel. Vivamus malesuada rhoncus felis, vitae volutpat sem mollis a. Nulla quis lorem id nibh tempus feugiat vitae quis augue. Praesent at eleifend sem, quis ullamcorper diam. Curabitur sed dictum tellus, sed egestas nibh. Cras ornare pulvinar nunc, eu pellentesque ante vulputate vitae. Nulla leo eros, iaculis quis consectetur in, hendrerit non diam. Proin auctor, magna et sagittis bibendum, enim justo scelerisque leo, sit amet rhoncus diam libero ac nisi. Maecenas sollicitudin, quam ut tincidunt maximus, turpis lorem feugiat nisi, ac pulvinar dolor magna id elit. Pellentesque odio ligula, varius eget magna in, vehicula auctor felis.\r\n\r\nIn ut nisl at risus lobortis lacinia ut non leo. Donec ut faucibus mauris. Curabitur tempus dignissim augue, ac ultrices diam blandit sed. Maecenas id neque ut ipsum dictum venenatis sed eget ex. Donec aliquet metus quis molestie mattis. Ut a venenatis nunc. Mauris mollis mauris et est posuere, eu porta dui vehicula. Nunc laoreet libero et diam sodales tincidunt. Maecenas luctus porta lorem, laoreet feugiat libero pretium eu. Morbi tincidunt finibus ullamcorper. Morbi a blandit eros. Mauris convallis erat dui, hendrerit congue odio posuere sit amet. Nulla in ipsum metus. Pellentesque quis hendrerit massa. Donec sodales luctus augue sed consectetur.', 2, 1, 1, '2019-01-04 06:31:27', '2019-01-04 06:31:27', 'ebooks/book/todd-quackenbush-701-unsplash(1).jpg', '2019-01-04', 'ebooks/image/1011245_orig.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `book_audiences`
--

CREATE TABLE `book_audiences` (
  `book_id` int(11) NOT NULL,
  `audience_id` int(11) NOT NULL,
  `last_updated` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_audiences`
--

INSERT INTO `book_audiences` (`book_id`, `audience_id`, `last_updated`) VALUES
(1, 1, '0000-00-00'),
(26, 1, '0000-00-00'),
(26, 2, '0000-00-00'),
(26, 3, '0000-00-00'),
(26, 4, '0000-00-00'),
(27, 1, '2019-01-04'),
(27, 2, '2019-01-04');

-- --------------------------------------------------------

--
-- Table structure for table `book_authors`
--

CREATE TABLE `book_authors` (
  `book_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `last_updated` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_authors`
--

INSERT INTO `book_authors` (`book_id`, `author_id`, `last_updated`) VALUES
(1, 1, '0000-00-00'),
(7, 1, '0000-00-00'),
(8, 1, '0000-00-00'),
(9, 1, '0000-00-00'),
(11, 1, '0000-00-00'),
(12, 1, '0000-00-00'),
(13, 1, '0000-00-00'),
(14, 1, '0000-00-00'),
(15, 1, '0000-00-00'),
(16, 1, '0000-00-00'),
(17, 2, '0000-00-00'),
(18, 1, '0000-00-00'),
(19, 1, '0000-00-00'),
(20, 1, '0000-00-00'),
(20, 2, '0000-00-00'),
(20, 3, '0000-00-00'),
(21, 1, '0000-00-00'),
(21, 4, '0000-00-00'),
(26, 1, '0000-00-00'),
(26, 2, '0000-00-00'),
(26, 3, '0000-00-00'),
(26, 4, '0000-00-00'),
(27, 1, '2019-01-04'),
(27, 3, '2019-01-04');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `dewi_decimal` varchar(3) NOT NULL,
  `description` text,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `dewi_decimal`, `description`, `is_active`, `date_created`, `date_updated`, `parent_id`) VALUES
(1, 'Computer Science, Knowledge and Systems', '001', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 'Philosophy and Psychology', '100', NULL, 1, '2018-11-29 00:00:00', '2018-11-29 00:00:00', NULL),
(3, 'Religion', '300', NULL, 1, '2018-11-29 00:00:00', '2018-11-29 00:00:00', NULL),
(4, 'Social Sciences', '300', NULL, 1, '2018-11-29 00:00:00', '2018-11-29 00:00:00', NULL),
(5, 'Language', '400', NULL, 1, '2018-11-29 00:00:00', '2018-11-29 00:00:00', NULL),
(6, 'Science', '500', NULL, 1, '2018-11-29 00:00:00', '2018-11-29 00:00:00', NULL),
(7, 'Technology', '600', NULL, 1, '2018-11-29 00:00:00', '2018-11-29 00:00:00', NULL),
(8, 'Arts and Recreation', '700', NULL, 1, '2018-11-29 00:00:00', '2018-11-29 00:00:00', NULL),
(9, 'Literature', '800', NULL, 1, '2018-11-29 00:00:00', '2018-11-29 00:00:00', NULL),
(10, 'History & geography', '900', NULL, 1, '2018-11-29 00:00:00', '2018-11-29 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `publishers`
--

CREATE TABLE `publishers` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `country` varchar(45) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `publishers`
--

INSERT INTO `publishers` (`id`, `name`, `country`, `is_active`, `date_created`, `date_updated`) VALUES
(1, 'Julie Pblications', 'Fiji', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'tex', '', 1, '2018-11-27 23:55:18', '2018-11-27 23:55:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `audiences`
--
ALTER TABLE `audiences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_book_category1_idx` (`category_id`),
  ADD KEY `fk_book_publisher1_idx` (`publisher_id`);

--
-- Indexes for table `book_audiences`
--
ALTER TABLE `book_audiences`
  ADD PRIMARY KEY (`book_id`,`audience_id`),
  ADD KEY `fk_book_has_audience_audience1_idx` (`audience_id`),
  ADD KEY `fk_book_has_audience_book_idx` (`book_id`);

--
-- Indexes for table `book_authors`
--
ALTER TABLE `book_authors`
  ADD PRIMARY KEY (`book_id`,`author_id`),
  ADD KEY `fk_book_has_author_author1_idx` (`author_id`),
  ADD KEY `fk_book_has_author_book1_idx` (`book_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_category_category1_idx` (`parent_id`);

--
-- Indexes for table `publishers`
--
ALTER TABLE `publishers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `audiences`
--
ALTER TABLE `audiences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `publishers`
--
ALTER TABLE `publishers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `fk_book_category1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_book_publisher1` FOREIGN KEY (`publisher_id`) REFERENCES `publishers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `book_audiences`
--
ALTER TABLE `book_audiences`
  ADD CONSTRAINT `fk_book_has_audience_audience1` FOREIGN KEY (`audience_id`) REFERENCES `audiences` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_book_has_audience_book` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `book_authors`
--
ALTER TABLE `book_authors`
  ADD CONSTRAINT `fk_book_has_author_author1` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_book_has_author_book1` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
